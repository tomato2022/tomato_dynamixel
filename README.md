tomato2022/tomato_dynamixel
===

tomato2022に用いるDynamixelモータ（特に昇降機の上げ下げに用いるもの）を制御するROSプロジェクトです。

昇降機のモータを回転させるための通信を行うROSノードとそれへの司令を送ることのできるROSトピックを用意しています。

## Installing
このプロジェクトは ROBOTIS-GIT/DynamixelSDK に依存しています。

```bash
$ cd ~/catkin_ws
$ git clone https://github.com/ROBOTIS-GIT/DynamixelSDK.git
$ git clone https://gitlab.com/tomato2022/tomato_dynamixel.git
$ catkin build
```

## How To Use
### launchファイルを使わずにノード起動
```bash
$ roscore
```
```bash
$ rosrun tomato_dynamixel dynamixel_controller
```

### launchファイルを使ってノード起動
```bash
$ roslaunch tomato_dynamixel controller.launch
```

## Implements
### Topics
```bash
$ rostopic list
...
/tomato_elevator/current
/tomato_elevator/velocity
/tomato_elevator/velocity_goal
```
#### /tomato_elevator/current
現在のモータに流れる電流値です。

#### /tomato_elevator/velocity
現在のモータの回転速度です。

#### /tomato_elevator/velocity_goal
モータの目標回転速度を指定できます。
例：
```bash
$ rostopic pub -1 /tomato_elevator/velocity_goal/ std_msgs/Int32 -- 100 #正回転
$ rostopic pub -1 /tomato_elevator/velocity_goal/ std_msgs/Int32 -- -100 #逆回転
```

### Keyboard Controll
テスト＆緊急用にキーボードからでも操作できます。
```
u: 正回転
d: 逆回転
Space: モータ回転開始/停止
```

## How to implement
実装方法について

**注意** dynamixelのポートハンドラはプログラム内に複数定義すると動かないという問題があります。
なのでこの昇降台を動かすためのポートハンドラは、チームで使っているプログラムで定義したものを流用するという形の実装になっています。

### Class
#### Dynamixel_controller
dynamixel_controller.cppとdynamixel_controller.hppの両方を使っているソースファイルのディレクトリに追加してください。

##### functions
```cpp
Dynamixel_controller::Dynamixel_controller(const char* device_name, const int del_elevator_id)
```
クラスのコンストラクタ。device_nameには"/dev/ttyUSB0"、del_elevator_idには昇降機に使われているdynamixelモータのIDを指定してください。


```cpp
Dynamixel_controller::Dynamixel_controller(const char* device_name, const int del_elevator_id, dynamixel::PortHandler* portHandler)
```
クラスのコンストラクタその２。device_nameには"/dev/ttyUSB0"、del_elevator_idには昇降機に使われているdynamixelモータのIDを指定してください。
portHandlerは上記の理由から、複数存在してはいけないのですでに用いているハンドラを与えてください。

```cpp
Dynamixel_controller::setVelocityGoal(int dxl_id, int goal)
```
昇降台の上げ下げする速度の指定をする関数。プラスの速度で上昇し、マイナスで下降する。
dxl_idは昇降機に使われているモータのID、goalは目標速度。

```cpp
Dynamixel_controller::getVelocity(int dxl_id)
```
昇降機の現在速度をゲットする関数。dxl_idは昇降機に使われているモータのID。

```cpp
Dynamixel_controller::getCurrent(int dxl_id)
```
昇降機の現在電流をゲットする関数。dxl_idは昇降機に使われているモータのID。

```cpp
Dynamixel::setVelocityCallback(const std_msgs::Int32::ConstPtr& vel, int dxl_id)
```
ROSのSubscriberに登録するためのコールバック関数。ただし、普通のROSコールバック関数は引数がひとつですが、設計上どうしてもふたつほしかったのでstd::bindを使った方法で登録しています。
参考：
```cpp
    ros::Subscriber ele_velocityGoal_sub = nh.subscribe<std_msgs::Int32>("/tomato/elevator/velocity_goal/", 10, std::bind(&Dynamixel_controller::setVelocityCallback, &controller,  std::placeholders::_1, DXL_ELEVATOR_ID));
```

