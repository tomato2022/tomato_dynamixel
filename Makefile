GEOMETRY = 160x40+1400+0
NEWTERM  = gnome-terminal --geometry=$(GEOMETRY) -- 

run:
	make build
	$(NEWTERM) roslaunch tomato_dynamixel controller.launch
build:
	catkin build tomato_dynamixel

roscore:
	$(NEWTERM) roscore
