#include "dynamixel_controller.hpp"
// Control table address

void Dynamixel_controller::dynamixel_initialization(const char* device_name){

    if(this->portHandler == nullptr){
        this->portHandler = dynamixel::PortHandler::getPortHandler(device_name);
    }
    this->packetHandler = dynamixel::PacketHandler::getPacketHandler(2.0);

    // check if dynamixel port open. If you cannot open port, try "sudo chmod 666 /dev/ttyUSB0"
    if (!portHandler->openPort()) {
        throw std::runtime_error("Failed to open the port!");
    }

    if (!portHandler->setBaudRate(PRO2::BAUDRATE)) {
        throw std::runtime_error("Failed to set the baudrate!");
    }

    for(auto&& dxl_id: this->dxl_ids){
        uint8_t dxl_error = 0;
        int dxl_comm_result = COMM_TX_FAIL;
        // change operating mode to velocity mode
        dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, dxl_id, PRO2::ADDR_OPERATING_MODE, PRO2::VELOCITY_MODE, &dxl_error);

        if (dxl_comm_result != COMM_SUCCESS) {
            std::string error_msg = "Failed to change mode for Dynamixel ID ";
            throw std::runtime_error(error_msg + std::to_string(dxl_id));
        }else{
            ROS_INFO("Success to change mode for Dynamixel ID %d", dxl_id); 
        }

        dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, dxl_id, PRO2::ADDR_TORQUE_ENABLE, 1, &dxl_error);
        if (dxl_comm_result != COMM_SUCCESS) {
            std::string error_msg = "Failed to enable torque for Dynamixel ID ";
            throw std::runtime_error(error_msg + std::to_string(dxl_id));
        }else{
            ROS_INFO("Success to enable torque for Dynamixel ID %d", dxl_id); 
        }
    }
}

Dynamixel_controller::Dynamixel_controller(const char* device_name, const std::vector<int> dxl_ids, dynamixel::PortHandler * portHandler)
    : dxl_ids(dxl_ids), portHandler(portHandler){
    for(auto&& id: dxl_ids){
        isMotorRunnable.insert(std::make_pair(id, false));
        motorVelocityGoal.insert(std::make_pair(id, 0));
        currentLimit.insert(std::make_pair(id, 100));
    }

    try{ 
        this->dynamixel_initialization(device_name);
    } catch(const std::runtime_error& e){
        ROS_ERROR("Dynamixel Initialization Error!!: %s", e.what());
        throw e;
    }
}

Dynamixel_controller::Dynamixel_controller(const char* device_name, const std::vector<int> dxl_ids)
    : Dynamixel_controller(device_name, dxl_ids, nullptr) {
}

Dynamixel_controller::~Dynamixel_controller(){
    int result = COMM_TX_FAIL;
    uint8_t dxl_error = 0;
    for(auto&& map: motorVelocityGoal){
        int dxl_id = map.first;
        result = packetHandler->write1ByteTxRx(portHandler, dxl_id, PRO2::ADDR_TORQUE_ENABLE, 0, &dxl_error);
        if (result != COMM_SUCCESS) {
            ROS_ERROR("Failed to disable torque for Dynamixel ID %d", dxl_id);
        }else{
            ROS_INFO("Success to disable torque for Dynamixel ID %d", dxl_id); 
        }
    }
    this->portHandler->closePort();
}

void Dynamixel_controller::setVelocityGoal(int dxl_id, int goal){
    this->motorVelocityGoal.at(dxl_id) = goal;
    updateMotorGoal(dxl_id);
}

void Dynamixel_controller::updateMotorGoal(int dxl_id){
    ///* velocity mode 
    int result = COMM_TX_FAIL;
    uint8_t dxl_error = 0;
    result = packetHandler->write4ByteTxRx(portHandler, dxl_id, PRO2::ADDR_GOAL_VELOCITY, isMotorRunnable.at(dxl_id)?motorVelocityGoal.at(dxl_id):0, &dxl_error);
    if (result != COMM_SUCCESS) {
        ROS_ERROR("Failed to set velocity! Result: %d", result);
    }
}

int32_t Dynamixel_controller::getVelocity(int dxl_id){
    uint32_t vel = 0;
    int result = COMM_TX_FAIL;
    uint8_t dxl_error = 0;
    result = packetHandler->read4ByteTxRx(portHandler, dxl_id, PRO2::ADDR_PRESENT_VELOCITY, &vel, &dxl_error);
    if (result != COMM_SUCCESS){
        ROS_ERROR("Failed to get velocity! Result: %d", result);
    }
    return *((int32_t*)&vel); // Recast the velocity value to int32_t since read4ByteTxRx returns uint32_t.
}

int16_t Dynamixel_controller::getCurrent(int dxl_id){
    uint16_t cur = 0;
    int result = COMM_TX_FAIL;
    uint8_t dxl_error = 0;
    result = packetHandler->read2ByteTxRx(portHandler, dxl_id, PRO2::ADDR_PRESENT_CURRENT, &cur , &dxl_error);
    if (result != COMM_SUCCESS){
        ROS_ERROR("Failed to get current! Result: %d", result);
    }
    return *((int16_t*)&cur); // Recast the current value to int16_t since read2ByteTxRx returns uint16_t.
}

void Dynamixel_controller::setMotorRunnability(int dxl_id, bool runnable){
    this->isMotorRunnable.at(dxl_id) = runnable;
    updateMotorGoal(dxl_id);
}

void Dynamixel_controller::toggleMotorRunnability(int dxl_id){
    this->isMotorRunnable.at(dxl_id) = !this->isMotorRunnable.at(dxl_id);
    updateMotorGoal(dxl_id);
}

void Dynamixel_controller::setVelocityCallback(const std_msgs::Int32::ConstPtr& vel, int dxl_id){
    this->setMotorRunnability(dxl_id, true);
    this->setVelocityGoal(dxl_id, (int)vel->data);
}

void Dynamixel_controller::stopIfTooBigTorque(){
    for(auto&& map: this->currentLimit){
        int dxl_id = map.first;
        uint16_t current_limit = map.second;
        int16_t cur = std::abs(this->getCurrent(dxl_id));
        this->currentLog.push_back(cur);
        if(this->currentLog.size() > 10){
            this->currentLog.erase(this->currentLog.begin());
        }
        if(cur > current_limit){
            ROS_ERROR("Torque is too big! ID:%d, Current:%d", dxl_id, cur);
            this->setMotorRunnability(dxl_id, false);
        }
    }
}

std::map<int, int> Dynamixel_controller::getVGoal(){
    return this->motorVelocityGoal;
}
