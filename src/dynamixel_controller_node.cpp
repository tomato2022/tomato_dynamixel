#include <unistd.h>
#include <stdio.h>
#include <termios.h>
#include <fcntl.h>
#include <string>
#include <array>
#include <functional>
#include <stdexcept>

#include <ros/ros.h>

#include "std_msgs/String.h"
#include "std_msgs/Int16.h"
#include "std_msgs/Int32.h"
#include "dynamixel_sdk/dynamixel_sdk.h"
#include "dynamixel_controller.hpp"

// Settings
constexpr int DXL_ELEVATOR_ID       = 0; // Using DXL'S ID for elevator
constexpr const char * DEVICE_NAME  = "/dev/ttyUSB0"; // [Linux] To find assigned port, use "$ ls /dev/ttyUSB*" command
constexpr int NODE_FREQUENCY        = 200;             // Hz


int kbhit(void)
{
    struct termios oldt, newt;
    int ch;
    int oldf;

    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

    ch = getchar();

    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    fcntl(STDIN_FILENO, F_SETFL, oldf);

    if (ch != EOF) {
        ungetc(ch, stdin);
        return 1;
    }

    return 0;
}


int main(int argc, char ** argv){
    std::vector<int> dxl_ids = {DXL_ELEVATOR_ID};
    Dynamixel_controller controller(DEVICE_NAME, dxl_ids);

    ros::init(argc, argv, "read_write_node");
    ros::NodeHandle nh;
    ros::Publisher ele_current_pub = nh.advertise<std_msgs::Int16>("/tomato/elevator/current/", 1);
    ros::Publisher ele_velocity_pub = nh.advertise<std_msgs::Int32>("/tomato/elevator/velocity/", 1);
    ros::Subscriber ele_velocityGoal_sub = nh.subscribe<std_msgs::Int32>("/tomato/elevator/velocity_goal/",
                                                                     10,
                                                                     std::bind(&Dynamixel_controller::setVelocityCallback, &controller, std::placeholders::_1, DXL_ELEVATOR_ID)
                                                                     );
    ros::Rate cycle_rate(NODE_FREQUENCY);

    char key = 0;

    while(ros::ok()){
        ros::spinOnce();
        if (kbhit()){
            key = getchar();
            switch (key){
            case ' ':
                controller.toggleMotorRunnability(DXL_ELEVATOR_ID);
                break;
            case 'd':
                controller.setVelocityGoal(DXL_ELEVATOR_ID, -200);
                break;
            case 'u':
                controller.setVelocityGoal(DXL_ELEVATOR_ID, 200);
                break;
            default:
                break;
             }
        }
        std_msgs::Int16 cur;
        cur.data = controller.getCurrent(DXL_ELEVATOR_ID);
        ele_current_pub.publish(cur);

        std_msgs::Int32 vel;
        vel.data = controller.getVelocity(DXL_ELEVATOR_ID);
        ele_velocity_pub.publish(vel);
    }

    return 0;
}
